package com.example.sony.recyclerview_json;
// класс модели данных. Включает в себя поля из объекта JSON и методы геттеры и сеттеры

public class Contact {
    private String fio; //фамилия
    private String birthday; //день рождения
    private String phone; //телефон

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getFio() {
        return fio;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }
}

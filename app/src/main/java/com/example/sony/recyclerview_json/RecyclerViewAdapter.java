package com.example.sony.recyclerview_json;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Непостредственно адаптер для RecyclerView
 */
public class RecyclerViewAdapter extends  RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Contact> contacts;
    public RecyclerViewAdapter(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    //заполнения данных viewHolder'a
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Contact contact = contacts.get(i);
        viewHolder.fio.setText(contact.getFio());
        viewHolder.birthday.setText(contact.getBirthday());
        viewHolder.phone.setText(contact.getPhone());

    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    //паттерн ViewHolder - в конструкторе класса происходит единичная идентификация всех View на лейауте  recyclerview_item
    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView fio;
        private TextView birthday;
        private TextView phone;


        public ViewHolder(View itemView) {
            super(itemView);
            fio = (TextView) itemView.findViewById(R.id.fio);
            birthday = (TextView) itemView.findViewById(R.id.birthday);
            phone = (TextView) itemView.findViewById(R.id.phone);

        }
    }
}

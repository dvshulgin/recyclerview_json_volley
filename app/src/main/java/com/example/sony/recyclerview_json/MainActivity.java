package com.example.sony.recyclerview_json;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.sony.recyclerview_json.fragments.MyFragment1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity  {
    Button rvButton, frameButton;
    MyFragment1 myFragment1;
    FragmentTransaction fTrans;



    // URL to get contacts JSON
    private static String urlJsonObj = "https://dl.dropboxusercontent.com/u/41356403/%D0%A5%D1%80%D0%B5%D0%BD%D1%8C/staff.json";

    RecyclerViewAdapter adapter;
    // JSON Node names
    private static final String TAG_CONTACTS = "staff";
    private static final String TAG_FIO = "fio";
    private static final String TAG_BIRTHDAY = "birthday";
    private static final String TAG_PHONE = "phone";

    //  JSONArray - в нем хранится массив объектов из JSON-запроса
    JSONArray jsonContacts = null;

    //массив контактов List, данные контакты получены из jsonContacts
    List<Contact> contacts = new ArrayList<Contact>();

    private void callJsonRequest (String urlJsonObj){
        //создаем очередь запроса volley
        RequestQueue queue = Volley.newRequestQueue(this);

        //создаем сам запрос объекта JSON, используя метод get
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                urlJsonObj,
                "",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            jsonContacts = response.getJSONArray(TAG_CONTACTS); //получаем массив оъектов "staff" из json-запроса
                            for (int i = 0; i < jsonContacts.length(); i++) { //получаем поля фио/день рождение/телефон из каждого объекта массива
                                JSONObject c = jsonContacts.getJSONObject(i);
                                String jsFio = c.getString(TAG_FIO);
                                String jsBirthday = c.getString(TAG_BIRTHDAY);
                                String jsPhone = c.getString(TAG_PHONE);

                                //создаем экземляр модели
                                Contact contact = new Contact();

                                //присваеваем полям экземпляра значения из полей объекта
                                contact.setFio(jsFio);
                                contact.setBirthday(jsBirthday);
                                contact.setPhone(jsPhone);

                                //заносим экземпляры модели в List
                                contacts.add(contact);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //говорим адаптеру, что нужно обновить список RecyclerView
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        // Adding request to request queue
        queue.add(request);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvButton = (Button) findViewById(R.id.rvButton);
        myFragment1 = new MyFragment1();

        frameButton= (Button) findViewById(R.id.frameButton);
        frameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fTrans = getFragmentManager().beginTransaction();
                fTrans.replace(R.id.container, myFragment1);
                fTrans.commit();

                Fragment frag1 = getFragmentManager().findFragmentById(R.id.refreshFragment);
                RecyclerView recyclerView =(RecyclerView) frag1.getView().findViewById(R.id.recyclerViewFragment);



                //RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewFragment);
                //создаем адаптер, лейаут менеджер и устанавливаем itemAnimator
                adapter = new RecyclerViewAdapter(contacts);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplication().getBaseContext());
                RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();

                //вызываем метод, который делает volley-запрос по определенному адресу и распарсивает json-файл
                callJsonRequest(urlJsonObj);

                //устанавливаеем для соответствующего RecyclerView адаптер, лейаутменеджер и итем-аниматор
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setItemAnimator(itemAnimator);



            }
        });




        //rvButton.setOnClickListener(this);
    }

/*
    @Override
    public void onClick(View v) {
//с помощью явного Intent вызываем RecyclerViewActivity
        Intent intent = new Intent(this, RecyclerViewActivity.class);
        startActivity(intent);
    }
*/
}
